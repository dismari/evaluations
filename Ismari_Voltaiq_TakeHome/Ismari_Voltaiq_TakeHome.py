
import pandas as pd
import matplotlib.pyplot as plt

class importer(object):
    '''
    importer 
        This class takes a dictionary of parameters as input. An instance of this class with appropriate parameters
        will take a given data file, strip specifed number of metadata header lines (relocating these to a text file),
        convert input data to a csv format (outputting this to a specified file location), and creating a plot of 
        specified x,y pairs.
            DEPENDENCEIS: pandas
    '''
    def __init__(self, params):        
        '''
        __init__
            When the importer class is instantiated, __init__ grabs passed in parameter dictionary, parses it, and calls
            the three workers that will extract the metadata, extract the csv, and lastly, plot the specified x,y columns
        '''        
        # Break the list of x,y pairs into a 2D array
        self.indexes = [params['plot'].split(",")[i:i+2] for i in range(0,len(params['plot'].split(",")),2)]
        self.xlabel = params['xlabel']
        self.ylabel = params['ylabel']
        self.outFile = params['fileOut']        
        self.inFile = params['fileIn']
        self.headerLines = params['HeaderLines']
        self.delimiter = params['delimiter']
        # Call extractMetaData() that will cull metadata lines into a separate text file
        self.extractMetaData()        
        # Call extractCSV() that will read in the data section of the file and write it out to a separate csv file
        self.extractCSV()         
        # Call the plotIt class passing in our modified importer class and PNG will be created 
        plotIt(self)        
        
    def extractMetaData(self):        
        '''
        extractMetaData
            Reads in a specified file, collecting specified number of header lines of metadata, and lastly, writing 
            this metadata out to a separate text file.            
        '''        
        with open(self.inFile) as fh:
            self.metaData = [ next(fh) for i in range(self.headerLines) ]
            fh.close()                
        outfh = open(self.outFile.replace('csv','txt'), 'w')
        outfh.write("".join(self.metaData))
        outfh.close()                            
        
    def extractCSV(self):
        '''
        extractCSV
            Reads in a data file offset by number of header lines representing metadata and parses the data given
            the specified delimiter. The data is then written out to a separate CSV file.
        '''        
        self.data = pd.read_csv(self.inFile, skiprows=self.headerLines, sep=self.delimiter, engine='python')
        self.data.to_csv(self.outFile)

class plotIt(object):
    '''
    plotIt
        Takes an importer object as input and when instantiated, generates a PNG containing waveforms of specified
        x,y pairs
            DEPENDENCIES: matplotlib, importer
    '''    
    def __init__(self, importObj):                          
        for x,y in importObj.indexes:
            plt.plot(importObj.data[x], importObj.data[y])
        plt.xlabel(importObj.xlabel)
        plt.ylabel(importObj.ylabel)
        plt.savefig(importObj.outFile.replace('csv','png'))
        plt.show()
        
file1 = importer({'fileIn':r'test_files\1\1.xyz','fileOut':r'results\file1.csv',\
                  'HeaderLines': 5,  'delimiter': ',',\
                  'plot':'Time [s],Voltage [V]',\
                  'xlabel':'Time (s)','ylabel':'Voltage (V)'})
## Verified metadata text file, csv data file, and png output file created!!

file2 = importer({'fileIn':r'test_files\2\2.xyz','fileOut':r'results\file2.csv',\
                   'HeaderLines': 15, 'delimiter': ',',\
                   'plot':'Run Time (h),Potential (V)',\
                   'xlabel':'Run Time (h)','ylabel':'Voltage (V)'})
## Verified metadata text file, csv data file, and png output file created!!

file3 = importer({'fileIn':r'test_files\3\3.xyz','fileOut':r'results\file3.csv',\
                  'HeaderLines': 34, 'delimiter': r'\t', 'plot':'TestTime,Voltage,TestTime,V_SP,TestTime,PbAdV',\
                  'xlabel':'Time (s)','ylabel':'Voltage (V)'})
## Verified metadata text file, csv data file, and png output file created!!

## ADDED MISSING TAB IN INPUT FILE IN COLUMN NAMES!!!
file4 = importer({'fileIn':r'test_files\4\4.xyz','fileOut':r'results\file4.csv',\
                   'HeaderLines': 1,  'delimiter': r'\t',\
                   'plot':'Test (Sec),Volts,Test (Sec),Aux #1,Test (Sec),Aux #2,Test (Sec),Aux #3,Test (Sec),Aux #4',\
                   'xlabel':'Time (s)','ylabel':'Voltage (V)'})
## Verified metadata text file, csv data file, and png output file created!!


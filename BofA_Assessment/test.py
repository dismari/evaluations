from __future__ import print_function
from itertools import count

import numpy as np
import pandas as pd

import torch
import torch.nn.functional as F

POLY_DEGREE = 4
W_target = torch.randn(POLY_DEGREE, 1) * 5
b_target = torch.randn(1) * 5


def make_features(x):
    """Builds features i.e. a matrix with columns [x, x^2, x^3, x^4]."""
    x = x.unsqueeze(1)
    return torch.cat([x ** i for i in range(1, POLY_DEGREE+1)], 1)


def f(x):
    """Approximated function."""
    return x.mm(W_target) + b_target.item()


def poly_desc(W, b):
    """Creates a string description of a polynomial."""
    result = 'y = '
    for i, w in enumerate(W):
        result += '{:+.2f} x^{} '.format(w, i + 1)
    result += '{:+.2f}'.format(b[0])
    return result


def get_batch(batch_size=32):
    """Builds a batch i.e. (x, f(x)) pair."""
    random = torch.randn(batch_size)
    x = make_features(random)
    y = f(x)
    return x, y


# Define model
fc = torch.nn.Linear(W_target.size(0), 1)

def getData( fp, targetStr ):
  data = pd.read_csv(fp, index_col=0)
  data = data.fillna(0)
  for title in data.columns.values:     
    if type(data[title][0]) == str:
      vocab = data[title].unique()
      data[title] = data[title].apply(lambda x: np.where(data[title].unique()==x)[0][0])
  target = data.pop(targetStr)
  return [data, target]

[trainData, trainTarget] = getData('train.csv', 'Target3')
[testData, testTarget] = getData('test.csv', 'Target3')  
for batch_idx in count(1):
    # Get data
    batch_x = trainData.sample(frac=1)[:50].to_numpy()
    batch_y = trainTarget.sample(frac=1)[:50].to_numpy()

    # Reset gradients
    fc.zero_grad()

    # Forward pass
    output = F.smooth_l1_loss(fc(batch_x), batch_y)
    loss = output.item()

    # Backward pass
    output.backward()

    # Apply gradients
    for param in fc.parameters():
        param.data.add_(-0.1 * param.grad)

    # Stop criterion
    if loss < 1e-3:
        break

print('Loss: {:.6f} after {} batches'.format(loss, batch_idx))
print('==> Learned function:\t' + poly_desc(fc.weight.view(-1), fc.bias))
print('==> Actual function:\t' + poly_desc(W_target.view(-1), b_target))

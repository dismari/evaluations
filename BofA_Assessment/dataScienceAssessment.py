
## PYTHON 3.7.9
"""
Task:

Please find attached two data sets. Your goal is to build a linear regression model to predict the variable Target3 using the other variables y1-y10.

Please do the following:

  - Use the data set train.csv to build your model
  - Use test.csv to evaluate your model

In your response please explain:

  - Your steps in building the model;
  - How you evaluate your models, using statistical metrics (R-squared, p-values, any other metrics you might consider, etc.);
  - Any concerns you might have with your approach.

For someone familiar with linear regression models this exercise should take 90 minutes.

Please submit your solution in Python (preferred) or R.

"""

## Written in Python 3.7.9 in an Anaconda 4.8.3 distribution on Windows 10 solution utilizing Tensorflow 2.1.0. 
## Took the time to research and experiment with Tensorflow (I had only completed tutorials previously). 
## Findings, analysis, and conclusion can be found at the bottom of this file.
## Author: Dylan Ismari, PhD, 10/1/2020

import sys
import os
import numpy as np
import pandas as pd
import tensorflow as tf

## getData takes a file path and a column header referencing the target data column in the file.
## -- Returns a pandas dataframe of the feature dataset and a pandas data series representing the labels/targets.
## Note: Empty  cells are read in by pandas as NaN and these are replaced with 0's.
def getData( fp, targetStr ):
  data = pd.read_csv(fp, index_col=0)
  data = data.fillna(0)
  target = data.pop(targetStr)
  return [data, target]
  
## buildFeatures takes in the dataframe as input with hardcoded column titles. These titles are split between
## categorical column data and numeric data. For categorical data, a vocabulary is captured by calling the 
## unique() method of a series. This enables an encoding in order to represent the category. The numeric data
## is set to a float32 data type (default for tensorflow).
## -- Returns a list of features of the tensorflow feature data type defining the features and well as 
##    their respective data types.
def buildFeatures( data ):
  CAT_COLUMNS = ['y8','y9']
  NUM_COLUMNS = ['y1','y2','y3','y4','y5','y6','y7','y10']
  features = []
  for label in CAT_COLUMNS:
    vocab = data[label].unique()
    features.append(tf.feature_column.categorical_column_with_vocabulary_list(label, vocab))
  for label in NUM_COLUMNS:  
    features.append(tf.feature_column.numeric_column(label, dtype=tf.float32))  
  return features

## buildTFData takes data features, target labels, and has defaults set for shuffle, batch count, and epochs
## and builds a tensorflow dataset structure utilizing the from_tensor_slices() method. This data structure 
## defines not only features and labels but also how the data will be used in the regression routines 
## (training/evaluation/prediction).
## -- Returns a tensorflow dataset data structure with data parameters set for regression routines.
def buildTFData(data, target, shuffle=True, batchCount=25, epochs=100): 
  def input_function():
    ds = tf.data.Dataset.from_tensor_slices((dict(data), target))
    if shuffle:
      ds = ds.shuffle(1000)
    ds = ds.batch(batchCount).repeat(epochs)
    return ds
  return input_function
  
## calcRsq takes evaluation targets/labels and predicted targets/labels and calculates the R-squared value 
## representing the coefficient of determination or how well the regression fits the data. A value of 1 is a 
## perfect fit with no residual error.
## -- Returns calculated R-squared value
def calcRsq(testTarget, testPrediction):
  meanTargets = (testTarget.sum())/len(testTarget)
  sumSquaresList = np.power(np.subtract(testTarget,meanTargets), 2)
  sumSquares = sumSquaresList.sum()
  sumResidualsList = np.power(np.subtract(testTarget,pd.Series(testPrediction)), 2)
  sumResiduals = sumResidualsList.sum()
  r_squared = 1 - (sumResiduals/sumSquares)
  return r_squared
  
## assessment is the main function that calculates a linear regression using Tensorflow estimator model generator.
## Outputs training error, evaluation error, some predicted values, and R-squared value.
def assessment(trainDataFile, testDataFile):
  # Build the pandas dataframes
  [trainData, trainTarget] = getData(trainDataFile, 'Target3')
  [testData, testTarget] = getData(testDataFile, 'Target3')  
  
  # Build the features list (column titles with data types)
  features = buildFeatures( trainData )
  
  # Build the tensorflow datasets with regression settings set.
  trainfn = buildTFData(trainData, trainTarget)
  testfn = buildTFData(testData, testTarget, epochs=1, shuffle=False)
    
  # Build the linear regression model referencing the feature list 
  linear_est = tf.estimator.LinearRegressor(features)
  
  # Train the linear regression model against the training dataset
  linear_est.train(trainfn)
  
  # Evaluate the training error of the linear regression model 
  trainResult = linear_est.evaluate(trainfn)
  
  # Evaluate the testing error of the linear regression model 
  testResult = linear_est.evaluate(testfn)
  print("trainResult", trainResult)
  print("testResult", testResult)
  
  # Collect predicted outputs of the linear regression model inputting the test data
  testPrediction = linear_est.predict(testfn)
  i = 0
  predictions = []
  for prediction in testPrediction:
    predictions.append(prediction["predictions"][0])
    i += 1
    if i < 10:
      print(prediction)      
  
  # Calculate the coefficient of determination (R-squared value)
  r_squared = calcRsq(testTarget, predictions)
  print("R-squared value: ", r_squared)

  return

## Catch to collect command line arguments and pass them to main function. 
if __name__=="__main__":
  training = sys.argv[1]
  testing  = sys.argv[2]
  assessment(training, testing)
  
####################################################################
####################################################################
####################################################################
## Example Output:
# trainResult {'average_loss': 1.5208541, 'label/mean': 0.54723734, 'loss': 1.5208576, 'prediction/mean': 0.5485983, 'global_step': 40000}
# testResult {'average_loss': 1.520859, 'label/mean': 0.54723984, 'loss': 1.5208592, 'prediction/mean': 0.54859555, 'global_step': 40000}
# {'predictions': array([1.7957412], dtype=float32)}
# {'predictions': array([1.4361446], dtype=float32)}
# {'predictions': array([3.34173], dtype=float32)}
# {'predictions': array([-2.6967015], dtype=float32)}
# {'predictions': array([2.5114923], dtype=float32)}
# {'predictions': array([2.8489566], dtype=float32)}
# {'predictions': array([-1.2881202], dtype=float32)}
# {'predictions': array([-2.1609974], dtype=float32)}
# {'predictions': array([1.7042419], dtype=float32)}
# R-squared value:  0.8966223711513344

## Analysis of the results and conclusions:
## The average loss represents the RMS error. The value of 1.5 is not great. Thus I went ahead and calculated
## the R-squared value to check how far from 1 we are. A value of 0.897 is okay but also would like better.
## My inexperience with tensorflow estimator model generation is most likely the case. I would suspect the
## assumption to make the blank values 0's might have distorted the results as well. The run time of the 
## training, evaluation, and prediction, was taking just over  10 minutes. With additional runtime and possibly
## experimenting with different shuffles and batch settings, a tradeoff for more runtime and better error values
## would be my expectation. In conclusion, if a R-squared value of roughly .9 is acceptable, then this implemented 
## model would do the job but in most of my experience, this finding would not be sufficient and with more time,
## configuration exploration would be required to find a better solution. I am glad I took the time to explore
## tensorflow for this task. The power and scope of this library is very impressive to me but my inexperience in
## use definitely contributed to the findings being less than desired.



  



from random import randint

"""
  Given an NxN grid find the fewest moves possible 
  for a chess knight to capture the chess queen. The initial
  positions of knight and queen are given as (x,y) pairs.


"""

##########################################################################################
##########################################################################################
##########################################################################################
##########################################################################################

class knightsTour(object):
  def __init__(self, knight=(1,1), queen=(1,1), grid=1):
    self.knight = knight
    self.queen = queen
    self.grid = grid
    self.numMoves = 0
    self.visited = {self.knightToKey(self.knight): ''}
    self.initMoves = ["up_left","up_right","down_left","down_right","left_up","left_down","right_up","right_down"]
    self.moves = [[x] + [knight] + [0] for x in self.initMoves ] 

    if self.knight[0] == self.queen[0] and self.knight[1] == self.queen[1]:
      self.result = 0
    elif self.knight[0] > 0 and self.knight[0] <= grid and \
         self.knight[1] > 0 and self.knight[1] <= grid and \
         self.queen[0] > 0 and self.queen[0] <= grid and \
         self.queen[1] > 0 and self.queen[1] <= grid and \
         self.grid > 3:
      self.result = self.analyze()
    else:
      self.result = -1

  def knightToKey(self, knight):
    return f'{knight[0]}_{knight[1]}'

  def analyze(self):
    while(True):
      nextMove, knight, numMoves = self.moves.pop(0)
      newKnight = getattr(self, nextMove)(knight)
      if newKnight[0] == self.queen[0] and newKnight[1] == self.queen[1]:
        return numMoves + 1
      elif newKnight[0] > 0 and newKnight[0] <= self.grid and \
           newKnight[1] > 0 and newKnight[1] <= self.grid and \
           self.knightToKey(newKnight) not in self.visited:
        self.visited[self.knightToKey(newKnight)] = ''
        self.moves = self.moves + [[x] + [newKnight] + [numMoves + 1] for x in self.initMoves ]
      elif len(self.moves) == 0:
        return -1
      else:
        continue

  def up_left(self, knight ):
    return (knight[0] - 1, knight[1] + 2)
  def up_right(self, knight):
    return (knight[0] + 1, knight[1] + 2)
  def down_left(self, knight):
    return (knight[0] - 1, knight[1] - 2)
  def down_right(self, knight):
    return (knight[0] + 1, knight[1] - 2)
  def left_up(self, knight):
    return (knight[0] - 2, knight[1] + 1)
  def left_down(self, knight):
    return (knight[0] - 2, knight[1] - 1)
  def right_up(self, knight):
    return (knight[0] + 2, knight[1] + 1)
  def right_down(self, knight):
    return (knight[0] + 2, knight[1] - 1)

  def debug(self):
    knight = (randint(1,8), randint(1,8))
    queen = (randint(1,8), randint(1,8))
    grid = 8
    
    self.__init__(knight, queen, grid)
    print('## DEBUG ## ', 'Knight:', knight, 'Queen:', queen, 'Grid:', grid, 'Result:', self.result)

##########################################################################################
##########################################################################################
##########################################################################################
##########################################################################################

import unittest

class TestKnightsTour(unittest.TestCase):
  def runFunc(self, queen, knight, grid):
    process = knightsTour(queen, knight, grid)
    return process

  def test_1(self):
    knight = (2,5)
    queen = (2,6)
    grid = 8
    process = self.runFunc(queen,knight,grid)
    expect =  3
    self.assertEqual(process.result, expect)

  def test_2(self):
    knight = (3,3)
    queen = (5,4)
    grid = 8
    process = self.runFunc(queen,knight,grid)
    expect =  1
    self.assertEqual(process.result, expect)

  def test_3(self):
    knight = (1,2)
    queen = (3,3)
    grid = 8
    process = self.runFunc(queen,knight,grid)
    expect =  1
    self.assertEqual(process.result, expect)

  def test_4(self):
    knight = (5,4)
    queen = (8,6)
    grid = 8
    process = self.runFunc(queen,knight,grid)
    expect =  3
    self.assertEqual(process.result, expect)

  def test_5(self):
    knight = (7,1)
    queen = (4,3)
    grid = 8
    process = self.runFunc(queen,knight,grid)
    expect =  3
    self.assertEqual(process.result, expect)

  def test_6(self):
    knight = (1,8)
    queen = (8,1)
    grid = 8
    process = self.runFunc(queen,knight,grid)
    expect =  6
    self.assertEqual(process.result, expect)

  def test_7(self):
    knight = (3,3)
    queen = (3,3)
    grid = 8
    process = self.runFunc(queen,knight,grid)
    expect =  0
    self.assertEqual(process.result, expect)

  def test_8(self):
    knight = (9,8)
    queen = (8,1)
    grid = 8
    process = self.runFunc(queen,knight,grid)
    expect =  -1
    self.assertEqual(process.result, expect)

  def test_9(self):
    knight = (4,4)
    queen = (1,1)
    grid = 4
    process = self.runFunc(queen,knight,grid)
    expect =   2 
    self.assertEqual(process.result, expect)

  def test_10(self):
    knight = ( 19, 19)
    queen = (1,5)
    grid = 20
    process = self.runFunc(queen,knight,grid)
    expect =  12
    self.assertEqual(process.result, expect)

##########################################################################################
##########################################################################################
##########################################################################################
##########################################################################################

if __name__ == '__main__':
  suite = unittest.TestLoader().loadTestsFromTestCase(TestKnightsTour)
  unittest.TextTestRunner(verbosity=2).run(suite)

  tour = knightsTour()
  tour.debug()


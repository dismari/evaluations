
# S and T are OCR strings. There are characters that are not indentified in OCR and replaced with '?'
# groups of '?' are replaced by their integer values of '?'
# S and T have characters [a-zA-Z0-9]
# Determine if S and T could come from the same string (True)
# If not, return False.

# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

import re

def solution(S, T):
    # write your code in Python 3.6
    # This function loops over the strings multiple times but never nested. So in the worst case,
    # the last loop has to visit every character of the strings to verify they are the same making
    # algorithmic efficiency O(n). Space efficiency is O(n) where in the worst case have a varying 
    # length array of digits dependent upon input strings. The reusage of input memory space for 
    # replacing data keep memory footprint as small as possible.
    
    # Get all multi-digit numbers from strings
    Snums = re.findall(r"\d+", S)
    Tnums = re.findall(r"\d+", T)
    
    # Replace multi-digit numbers with that digit of known values '.'
    for num in Snums:
        S = re.sub(num,int(num)*'.',S)
    for num in Tnums:
        T = re.sub(num,int(num)*'.',T)

    # If they are not the same length here, then the strings are not from same text (short cut)    
    if len(S) != len(T):        
        return False

    # Check per character if strings match or if they were unknown, a single miss will return False
    for charS,charT in zip(S,T):
        if charS == charT or charS == '.' or charT == '.':
            pass
        else:
            return False    
    return True


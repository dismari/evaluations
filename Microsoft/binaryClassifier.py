
import pandas

"""
  Given the model and expected results from a binary 
  classifier below, report the quality of the model.
"""
from random import randint



model = [randint(0,1) for i in range(500)]
expected = [randint(0,1) for i in range(500)]

df = pandas.DataFrame({'model': model, 'expected':expected})

trues  = len(df[ (df['model'] == 1) & (df['expected'] == 1) ])
falses = len(df[ (df['model'] == 0) & (df['expected'] == 0) ])
false_trues  = len(df[ (df['model'] == 1) & (df['expected'] == 0) ])
false_falses = len(df[ (df['model'] == 0) & (df['expected'] == 1) ])

true_positives = trues /(trues+ false_trues)
true_negatives = falses/(falses+false_falses)
precision = (trues)/(trues+false_trues)
recall = (trues)/(trues+false_falses)
specificity = trues/(trues+false_trues)
accuracy = (trues+falses)/len(df)

f1 = 2 * recall * precision / (recall + precision)

print(f'Trues: {trues} Falses: {falses} False Trues: {false_trues} False Falses: {false_falses}')
print(f'Total: {trues+falses+false_trues+false_falses}')

print(f'True Positives: {true_positives}')
print(f'True Negatives: {true_negatives}')
print(f'Precision: {precision}')
print(f'Recall: {recall}')
print(f'Specificity: {specificity}')
print(f'Accuracy: {accuracy}')

print('')
print(f'F1-score: {f1}')




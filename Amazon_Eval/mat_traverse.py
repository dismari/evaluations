
import unittest

class TestTraverseMatrix(unittest.TestCase):
  def runFunc(self, matrix):
    process = traverseMat(matrix)
    process.processMat()
    #print(matrix)
    #print(process.vector)
    return process

  def test_3x4(self):
    matrix = [
      [  1,  3,  4,  9 ],
      [  2,  5,  8, 10 ], 
      [  6,  7, 11, 12 ],
    ]   
    process = self.runFunc(matrix)
    expect =  [ i+1 for i in range(0,3*4) ] 
    self.assertEqual(process.vector, expect)

  def test_4x4(self):
    matrix = [
      [  1,  3,  4, 10 ],
      [  2,  5,  9, 11 ], 
      [  6,  8, 12, 15 ],
      [  7, 13, 14, 16 ]
    ]   
    process = self.runFunc(matrix)
    expect =  [ i+1 for i in range(0,4*4) ] 
    self.assertEqual(process.vector, expect)

  def test_4x5(self):
    matrix = [
      [  1,  3,  4, 10, 11 ],
      [  2,  5,  9, 12, 17 ], 
      [  6,  8, 13, 16, 18 ],
      [  7, 14, 15, 19, 20 ]
    ]
    process = self.runFunc(matrix)
    expect =  [ i+1 for i in range(0,4*5) ] 
    self.assertEqual(process.vector, expect)
    

"""
[ [  1,  3,  4, 10 ],
  [  2,  5,  9, 11 ], 
  [  6,  8, 12, 15 ],
  [  7, 13, 14, 16 ]
]

  [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ]
"""
class traverseMat(object):
  def __init__(self, matrix):
    self.i = 0
    self.j = 0
    self.matrix = matrix
    self.max_i = len(self.matrix)
    self.max_j = len(self.matrix[0])
    self.numEls = self.max_i * self.max_j
    self.vector = []
    self.direction = 'DOWN'

  def down(self):
    self.checkIndexes(i=self.i+1, j=self.j)
  
  def down_left(self):
    self.checkIndexes(i=self.i+1, j=self.j-1)
  
  def up(self):
    self.checkIndexes(i=self.i-1, j=self.j )
  
  def up_right(self):
    self.checkIndexes(i=self.i-1, j=self.j+1)

  def right(self):
    self.checkIndexes(i=self.i, j=self.j+1)

  def updateDirection(self):
    self.direction = 'DOWN' if self.direction != 'DOWN' else 'RIGHT'
    
  def checkIndexes(self, i, j ):
    if i < 0 or j < 0 or i >= self.max_i or j >= self.max_j:
      raise Exception(f'Index out of bounds {i} {j}')
    self.i = i
    self.j = j
  
  def processMat(self):
    while( len(self.vector) < self.numEls ):
      self.vector.append( self.matrix[self.i][self.j] )
      if self.direction == 'DOWN':
        try:
          self.down_left()
          continue
        except Exception as e:
          try:
            self.down()
            self.updateDirection()
            continue 
          except Exception as e:
            try:
              self.right()
              self.updateDirection()
              continue
            except:
              pass
      else:
        try:
          self.up_right()
          continue
        except:
          try:
            self.right()
            self.updateDirection()
            continue
          except:
            try:
              self.down()
              self.updateDirection()
              continue
            except:
              pass


if __name__ == '__main__':
  suite = unittest.TestLoader().loadTestsFromTestCase(TestTraverseMatrix)
  unittest.TextTestRunner(verbosity=2).run(suite)


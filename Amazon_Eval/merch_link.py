

import unittest

class TestMerchantRelations(unittest.TestCase):
  def buildMerchants(self, merchants, relations ):
    m = merchantsClass( merchants )
    [ m.sellsTo( relation[0], relation[1]) for relation in relations ]
    #print(m.table)
    return m

  def merchants1(self):
    return self.buildMerchants('ABCDEFG', ['AB','BC','BE','CD','CE','CF','EG'])
  def merchants2(self):
    return self.buildMerchants('ABCD', ['AB','BC','BD'] )

  def test_1(self):
    m = self.merchants1()
    self.assertEqual(m.check('A','B'), (True, 'DIRECT'))
    self.assertEqual(m.check('A','C'), (True, 'INDIRECT'))
    self.assertEqual(m.check('A','E'), (True, 'INDIRECT'))
    self.assertEqual(m.check('B','E'), (True, 'DIRECT'))
    self.assertEqual(m.check('C','G'), (False, 'UNRELATED'))
    self.assertEqual(m.check('A','G'), (False, 'UNRELATED'))
    self.assertEqual(m.check('E','A'), (False, 'UNRELATED'))
    self.assertEqual(m.check('E','G'), (True, 'DIRECT'))
    self.assertRaises(Exception,m.check,'I','B')
  def test_2(self):
    m = self.merchants2()
    self.assertEqual(m.check('A','B'), (True, 'DIRECT'))
    self.assertEqual(m.check('A','C'), (True, 'INDIRECT'))
    self.assertRaises(Exception, m.check,'A','E')
    self.assertEqual(m.check('B','D'), (True, 'DIRECT'))
    self.assertEqual(m.check('A','D'), (True, 'INDIRECT'))
    self.assertEqual(m.check('C','D'), (False, 'UNRELATED'))
    self.assertEqual(m.check('B','A'), (False, 'UNRELATED'))
    self.assertRaises(Exception,m.check,'I','B')

"""
  merch(A) -> merch(B)
  merch(B) -> merch(C)
  merch(C) -> merch(D)
  merch(C) -> merch(E)
  merch(C) -> merch(F)
  merch(C) -> merch(E)
  merch(E) -> merch(G)

  A,F: A->B->C->D->E->F 'INDIRECT'
  C,F: C->F 'DIRECT'
  E,G: E->G 'DIRECT'
  C,G: UNRELATED
"""

class merchantsClass(object):
  def __init__(self, possibleMerchants):
    self.table = {} 
    for el in possibleMerchants:
      self.table[el] = []

  def sellsTo(self, seller, buyer):
    self.table[seller].append(buyer)

  def check(self, seller, buyer, initial=''):
    if initial == '' and not (seller in self.table.keys() and buyer in self.table.keys()):
      raise Exception( f'UNKNOWN MERCHANT')
    else:
      initial = seller if initial == '' else initial
      if buyer in self.table[seller] and initial == seller:
        return True, 'DIRECT'
      elif buyer in self.table[seller]:
        return True, 'INDIRECT'
      elif len( self.table[seller] ) > 0:
        for el in self.table[seller]:
          return self.check(el, buyer, initial)
      else:
        return False, 'UNRELATED'

if __name__ == '__main__':
  suite = unittest.TestLoader().loadTestsFromTestCase(TestMerchantRelations)
  unittest.TextTestRunner(verbosity=2).run(suite)



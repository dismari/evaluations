
import time 
from collections import OrderedDict

import unittest

class TestLRU(unittest.TestCase):
  def test_1(self):
    self.lru = lruClass()
    self.lru.insert('A','AAAA',1)
    self.assertEqual(self.lru.fetch('A'), 'AAAA')
    self.lru.insert('B','BBBB',1)
    self.lru.insert('C','CCAA',1)
    self.lru.insert('D','DDAA',1)
    self.assertEqual(self.lru.fetch('D'), 'DDAA')
    self.lru.insert('E','EEAA',1)
    self.assertEqual(self.lru.fetch('A'), None)
    self.assertEqual(self.lru.fetch('E'), 'EEAA')
    time.sleep(1)
    self.assertEqual(self.lru.fetch('B'), None)
    self.lru.insert('A','AAABA',1)
    self.assertEqual(self.lru.fetch('A'), 'AAABA')

class lruClass(object):
  def __init__(self, maxElements=3):
    self.maxElements = maxElements
    self.lru = OrderedDict() 
  
  def evict(self):
    self.lru.popitem(last=False)

  def insert(self, key, data, TTL):
    if len(self.lru) > self.maxElements:
      self.evict()
    self.lru[key] = {'data':data, 'expiration': int(time.time())+TTL }

  def fetch(self, key):
    try:
      el = self.lru[key]
      if el['expiration'] > int(time.time()):
        return el['data']
      raise Exception('Stale Data')
    except:
      return None

if __name__ == '__main__':
  suite = unittest.TestLoader().loadTestsFromTestCase(TestLRU)
  unittest.TextTestRunner(verbosity=2).run(suite)


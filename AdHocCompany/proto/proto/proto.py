
"""
  proto.py -- Read in a binary file and report specific findings.
    dependencies -- numpy
    Ran in Windows 10 Anaconda 3 Environment.
    Usage: python .\proto.py
           python .\proto.py <MPS7_binaryFilePath>
           python .\proto.py .\txnlog.dat
  Expected Output:
```
total credit amount=9366.02
total debit amount=18203.70
autopays started=10
autopays ended=8
balance for user 2456938384156277127=248.58
```
"""

import numpy as np

class record(object):
  """
    Record class which will initially capture the type and after
    expected to call update with appropriate number of bytes passed
  """
  def __init__(self, byte):
    self.type = 'Debit' if byte == 0 else \
                'Credit' if byte == 1 else \
                'StartAutopay' if byte == 2 else \
                'EndAutopay' #if byte == 3
                
  def updateRecord(self, byteList):
    dt = np.dtype(int)
    dt = dt.newbyteorder('>')    
    self.timeStamp = np.frombuffer(byteList[0:4], dtype=np.dtype('>u4'))[0]
    self.userID = np.frombuffer(byteList[4:12], dtype=np.dtype('>u8'))[0]
    try:
      self.amount = np.frombuffer(byteList[12:20], dtype=np.dtype('>f8'))[0]
    except:
      self.amount = float("NaN")

class mps7Class(object):
  """
    mps7Class class which builds out the data object when called from 
      MPS7 binary file path.
      Methods:
        checkAmount -- returns summed amounts of specified type 
        countRecords -- return count of specified record type 
        checkBalance -- returns amount associated with specified userID
        
  """
  def __init__(self,fp = "txnlog.dat"):
    self.data = (open(fp, 'rb')).read()    
    dt = np.dtype(int)
    dt = dt.newbyteorder('>')    
    self.header = {
      'magicStr': str(self.data[0:4],'utf-8'),
      'version': self.data[4],
      'numRecords': np.frombuffer(self.data[5:9], dtype=np.dtype('>u4'))[0]
    }
    self.records = []
    curInd = 9
    for rec in range(self.header['numRecords']):
      self.records.append(record(self.data[curInd]))
      curInd += 1
      if self.records[-1].type == 'Debit' or self.records[-1].type == 'Credit':
        self.records[-1].updateRecord(self.data[curInd:curInd+20])
        curInd += 20
      else:
        self.records[-1].updateRecord(self.data[curInd:curInd+12])
        curInd += 12
        
  def checkAmount(self, type):
    amount = 0
    for el in self.records:
      if el.type == type:
        amount += el.amount
    return amount

  def countRecords(self, type):
    count = 0
    for el in self.records:
      if el.type == type:
        count += 1
    return count
  
  def checkBalance(self, userID):
    for el in self.records:
      if el.userID == userID:
        return el.amount

def proto(binFilePointer = "txnlog.dat"):
  mps7 = mps7Class(binFilePointer)    
  creditAmount = "{:.2f}".format(mps7.checkAmount('Credit'))
  debitAmount = "{:.2f}".format(mps7.checkAmount('Debit'))
  autopayStarts = mps7.countRecords('StartAutopay')    
  autopayEnded = mps7.countRecords('EndAutopay')    
  userID = 2456938384156277127
  balance = "{:.2f}".format(mps7.checkBalance(userID))

  print(f"""
  ```
  total credit amount={creditAmount}
  total debit amount={debitAmount}
  autopays started={autopayStarts}
  autopays ended={autopayEnded}
  balance for user {userID}={balance}
  ```
  """)
  return

if __name__ == '__main__':
  try:
    proto(binFilePointer = sys.argv[1])
  except:
    proto()
    
    
    
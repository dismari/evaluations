
"""
  slcsp.py -- Second lowest cost silver plan function. Reads in plans, zipcodes, and slcsp csv files. 
      Completes the slcsp csv data correlating the zipcodes from the zipcodes file and finding the  
      second lowest silver plan rate in the plans file for that zipcode
    dependencies: pandas
    usage: python .\slcsp.py
    Ran on Windows 10 Anaconda 3 Environment
  Expected Output:
64148,245.20
67118,212.35
40813,
18229,231.48
51012,252.76
79168,243.68
54923,255.26
67651,249.44
49448,221.63
27702,283.08
47387,326.98
50014,287.30
33608,268.49
6239,
54919,243.77
46706,272.81
14846,
48872,280.47
43343,260.94
77052,243.72
7734,
95327,
12961,
26716,291.76
48435,248.99
53181,306.56
52654,242.39
58703,297.93
91945,
52146,254.56
56097,
21777,
42330,
38849,285.69
77586,243.72
39745,265.73
3299,240.45
63359,301.87
60094,209.95
15935,184.97
39845,325.64
48418,248.99
28411,307.51
37333,219.29
75939,234.50
7184,
86313,292.90
61232,222.38
20047,
47452,329.61
31551,290.60  
"""

import pandas as pd

class plansClass(object):
  def __init__(self, planFP = 'plans.csv'):
    super(plansClass,self).__init__() 
    self.plans = pd.read_csv(planFP)

class zipsClass(object):
  def __init__(self, zipsFP = 'zips.csv'):
    super(zipsClass,self).__init__()   
    self.zips = pd.read_csv(zipsFP)

class slcspClass(plansClass, zipsClass):
  def __init__(self, slcspFP = 'slcsp.csv'):
    super(slcspClass,self).__init__()   
    self.slcsp = pd.read_csv(slcspFP, header = 0)    
    for i, el in self.slcsp.iterrows():      
      zi = (self.zips.loc[ self.zips['zipcode'] == el['zipcode'] ]).iloc[0]
      try:
        rate = "{:.2f}".format(sorted(set((self.plans.loc[(self.plans['rate_area'] == zi['rate_area']) & \
                              (self.plans['state'] == zi['state']) & \
                              (self.plans['metal_level'] == 'Silver')\
                              ] )['rate']))[1])
      except:
        rate = ""
      print(f"{zi['zipcode']},{rate}")
      
slcsp = slcspClass()    
